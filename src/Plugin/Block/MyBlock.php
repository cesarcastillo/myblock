<?php

namespace Drupal\MyBlock\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'MyBlock' example block.
 *
 * @Block(
 *  id = "myblock",
 *  admin_label = @Translation("My Block"),
 * )
 */
class MyBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
          '#theme' => 'my_block', // Id for the theme we are passing machine name
          '#name' => 'My Block'
      ];
  }
}
